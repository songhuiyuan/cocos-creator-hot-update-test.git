const { ccclass, property } = cc._decorator;

@ccclass
export default class Helloworld extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property
    text: string = 'hello';



    start() {
        // init logic
        this.label.string = this.text;
        //this.openCountDownTime();
    }

    //倒计时模块
    private openCountDownTime() {
        let time: number = 6;
        this.label.string = time?.toString();
        let callback: Function = () => {
            if (time <= 0) {
                time = 0;
                this.loadSpriteAsset();
                return this.unschedule(callback);//倒计时完成
            }
            time--;
            cc.error(time);
            this.label.string = Math.max(time, 0)?.toString();
        }
        this.schedule(callback, 1);
    }

    private loadSpriteAsset(): void {
        let path: string = 'sprite1';
        let sprite: cc.Sprite = cc.find('cocos', this.node)?.getComponent(cc.Sprite);
        cc.resources.load(path, cc.SpriteFrame, (err: Error, spriteFrame: cc.SpriteFrame) => {
            if (sprite) {
                sprite.spriteFrame = spriteFrame;
            }
        })
    }
}
